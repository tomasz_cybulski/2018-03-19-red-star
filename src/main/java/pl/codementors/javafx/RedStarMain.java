package pl.codementors.javafx;

import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class RedStarMain extends Application {

    @Override
    public void start(Stage stage){
        BorderPane borderPane = new BorderPane();

        Scene scene = new Scene(borderPane, 600, 600, Color.BLACK);
        stage.setScene(scene);
        stage.show();

        VBox left = new VBox();

        Pane center = new Pane();
        Polygon star = new Polygon(
                13, 63,
                20, 39,
                0, 24,
                25, 24,
                33, 0,
                41, 24,
                67, 24,
                46, 39,
                54, 63,
                33, 48);

        star.setFill(Color.RED);
        star.layoutXProperty().bind(center.widthProperty().subtract(
                star.layoutBoundsProperty().get().getWidth()).divide(2));
        star.layoutYProperty().bind(center.heightProperty().subtract(
                star.layoutBoundsProperty().get().getHeight()).divide(2));

        center.getChildren().add(star);

        ToggleButton tb = new ToggleButton("Start/Stop");
        tb.setMaxHeight(Double.POSITIVE_INFINITY);
        left.setVgrow(left, Priority.ALWAYS);
        left.getChildren().add(tb);
        Transition transition = sequentialTransition(star);
        transition.pause();
        tb.setOnAction(event -> {
            if (tb.isSelected()) {
                transition.play();
            } else {
                transition.pause();
            }
        });

        borderPane.setLeft(left);
        borderPane.setCenter(center);
    }

    private Transition translateTransition1(Node node) {
        TranslateTransition st = new TranslateTransition();
        st.setDuration(Duration.millis(2000));
        st.setNode(node);
        st.setFromX(0);
        st.setFromY(0);
        st.setToX(0);
        st.setToY(-267);
        return st;
    }

    private Transition rotateTransition(Node node) {
        RotateTransition rt = new RotateTransition();
        rt.setAutoReverse(true);
        rt.setFromAngle(0);
        rt.setToAngle(360);
        rt.setDuration(Duration.millis(2000));
        rt.setNode(node);
        return rt;
    }

    private Transition translateTransition2(Node node) {
        TranslateTransition st = new TranslateTransition();
        st.setDuration(Duration.millis(2000));
        st.setNode(node);
        st.setFromX(0);
        st.setFromY(-267);
        st.setToX(235);
        st.setToY(0);
        return st;
    }

    private Transition parallelTransition1 (Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(translateTransition2(node));
        pt.getChildren().add(rotateTransition(node));;
        return pt;
    }

    private Transition translateTransition3(Node node) {
        TranslateTransition st = new TranslateTransition();
        st.setDuration(Duration.millis(2000));
        st.setNode(node);
        st.setFromX(235);
        st.setFromY(0);
        st.setToX(0);
        st.setToY(267);
        return st;
    }

    private Transition fadeTransition(Node node) {
        FadeTransition ft = new FadeTransition(Duration.millis(2000), node);
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
        ft.setAutoReverse(true);
        return ft;
    }

    private Transition parallelTransition2 (Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(translateTransition3(node));
        pt.getChildren().add(fadeTransition(node));;
        return pt;
    }

    private Transition translateTransition4(Node node) {
        TranslateTransition st = new TranslateTransition();
        st.setDuration(Duration.millis(2000));
        st.setNode(node);
        st.setFromX(0);
        st.setFromY(267);
        st.setToX(-235);
        st.setToY(0);
        return st;
    }

    private Transition fadeTransition2(Node node) {
        FadeTransition ft = new FadeTransition(Duration.millis(2000), node);
        ft.setFromValue(0.1);
        ft.setToValue(1.0);
        ft.setAutoReverse(true);
        return ft;
    }

    private Transition parallelTransition3(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(translateTransition4(node));
        pt.getChildren().add(fadeTransition2(node));;
        return pt;
    }

    private Transition translateTransition5(Node node) {
        TranslateTransition st = new TranslateTransition();
        st.setDuration(Duration.millis(2000));
        st.setNode(node);
        st.setFromX(-235);
        st.setFromY(0);
        st.setToX(0);
        st.setToY(0);
        return st;
    }

    private Transition fillTransition1(Shape shape) {
        FillTransition ft = new FillTransition();
        ft.setFromValue(Color.RED);
        ft.setToValue(Color.BLUE);
        ft.setShape(shape);
        ft.setAutoReverse(true);
        return ft;
    }

    private Transition fillTransition2(Shape shape){
        FillTransition ft2 = new FillTransition();
        ft2.setFromValue(Color.BLUE);
        ft2.setToValue(Color.RED);
        ft2.setShape(shape);
        ft2.setDuration(Duration.millis(1000));
        return ft2;
    }

    private Transition sequentialFillTransition(Shape node){
        SequentialTransition st = new SequentialTransition();
        st.getChildren().addAll(fillTransition1(node),
                fillTransition2(node));
        for (Animation a : st.getChildren()) {
            a.setCycleCount(1);
        }
        return st;
    }

    private Transition parallelTransition4(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(translateTransition5(node));
        pt.getChildren().add(sequentialFillTransition(node));
        return pt;
    }

    private Transition sequentialTransition(Shape node) {
        SequentialTransition st = new SequentialTransition();
        st.getChildren().addAll(translateTransition1(node),
                parallelTransition1(node),
                parallelTransition2(node),
                parallelTransition3(node),
                parallelTransition4(node));
        for (Animation a : st.getChildren()) {
            a.setCycleCount(1);
        }
        st.setCycleCount(Timeline.INDEFINITE);
        return st;
    }

    public static void main(String[] args){
        launch(args);
    }
}
